# README #

This is Daniel Stelzer (Draconis)'s repository of Dialog extensions. All are released with no warranty. Use at your own risk.

Licensing: CC-BY. In other words, do whatever you want with these, for any purpose, just credit me somehow. (If you haven't altered Dialog's default VERSION command, this will be taken care of automatically.)